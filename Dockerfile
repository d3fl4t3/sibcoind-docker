FROM ubuntu:latest
RUN apt update && apt install -y libboost-all-dev libssl-dev libprotobuf-dev protobuf-compiler libqt4-dev libqrencode-dev libtool autoconf build-essential wget git bsdmainutils pkg-config libevent-dev
RUN mkdir /bdb && cd /bdb && \
    wget http://download.oracle.com/berkeley-db/db-4.8.30.NC.tar.gz && \
    echo '12edc0df75bf9abd7f82f821795bcee50f42cb2e5f76a6a281b85732798364ef db-4.8.30.NC.tar.gz' | sha256sum -c && \
    tar -xvf db-4.8.30.NC.tar.gz && \
    cd db-4.8.30.NC/build_unix && \
    mkdir -p build && \
    BDB_PREFIX=$(pwd)/build && \
    ../dist/configure --disable-shared --enable-cxx --with-pic --prefix=$BDB_PREFIX && \
    make install
RUN cd / && git clone https://github.com/ivansib/sibcoin
RUN cd /sibcoin && mkdir /sibcoin-dest && ./autogen.sh && ./configure --prefix /sibcoin-dest --with-gui=no --with-utils=no CPPFLAGS="-I/bdb/db-4.8.30.NC/build_unix/build/include/ -O2" LDFLAGS="-L/bdb/db-4.8.30.NC/build_unix/build/lib/" && make && make install
RUN mkdir /wallet
ENTRYPOINT ["/sibcoin-dest/bin/sibcoind", "-datadir=/wallet", "-printtoconsole"]
